# Third party Libraries


## Contents

## Library for imagemagick version 7.0.9-2

```
wget https://bitbucket.org/KishukG/third-party-libraries/raw/2e21ca451e6a5c804bae0a2f61c99c95f170d8f7/ImageMagick.tar.gz
tar xvzf ImageMagick.tar.gz
cd ImageMagick-7.0.9-2/
touch configure
./configure
make
make install
sudo apt-get install libmagickwand-dev
```

# add these in bash profile
```
pkg-config --cflags --libs MagickWand
export CGO_CFLAGS_ALLOW='-Xpreprocessor'
```